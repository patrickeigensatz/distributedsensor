# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

from django.db import models
from django.contrib.auth.models import User


SENSOR_PING_DATA_TYPE_CHOICES = (
    ('NUM', 'Numeric'),
    ('TXT', 'Text')
)



class Sensor(models.Model):
    """An registered instance of a sensor that is configured to continuously ping a master"""
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{} (id={})".format(self.name, self.id)



class SensorPing(models.Model):
    sensor = models.ForeignKey(Sensor, related_name="pings", db_index=True, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} at {}".format(self.sensor.name, self.time)



class SensorPingDataField(models.Model):
    sensor = models.ForeignKey(Sensor, related_name="data_fields", db_index=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    data_type = models.CharField(max_length=3, choices=SENSOR_PING_DATA_TYPE_CHOICES, default="TXT")


    def __str__(self):
        if self.data_type == "TXT":
            return self.name + " (text) on " + self.sensor.name
        elif self.data_type == "NUM":
            return self.name + " (numeric) on " + self.sensor.name
        else:
            return self.name + " (unknown type) on " + self.sensor.name




class SensorPingData(models.Model):
    sensor_ping = models.ForeignKey(SensorPing, related_name="data", db_index=True, on_delete=models.CASCADE)
    field = models.ForeignKey(SensorPingDataField, related_name="data", db_index=True, on_delete=models.CASCADE)
    data = models.CharField(max_length=10*1024)


    def __str__(self):
        return "{}: {}={}".format(self.sensor_ping.time, self.field.name, self.data[:16])




class AuthenticationToken(models.Model):
    name = models.CharField(max_length=64)
    key = models.CharField(max_length=128)
    user = models.OneToOneField(User, default=None, blank=True, null=True, related_name="auth_token", on_delete=models.CASCADE)

    is_admin = models.BooleanField(default=False)
    read_permission = models.ManyToManyField(Sensor, default=None, blank=True, related_name="read_access_tokens")
    write_permission = models.ManyToManyField(Sensor, default=None, blank=True, related_name="write_access_tokens")


    def __str__(self):
        if self.is_admin:
            return "{} (admin)".format(self.name)
        else:
            return "{} (regular user)".format(self.name)



    @property
    def get_read_allowed_sensors(self):
        """
        Get all sensors that a given authentication key has read access to.

        Basically returns AuthenticationToken.read_permission but returns
        everything for admin tokens.

        :return: A QuerySet containing the sensors
        """

        if self.is_admin:
            return Sensor.objects.all()
        else:
            return self.read_permission.all()


    @property
    def get_write_allowed_sensors(self):
        """
        Get all sensors that a given authentication key has write access to.

        Basically returns AuthenticationToken.write_permission but returns
        everything for admin tokens.

        :return: A QuerySet containing the sensors
        """

        if self.is_admin:
            return Sensor.objects.all()
        else:
            return self.read_permission.all()





