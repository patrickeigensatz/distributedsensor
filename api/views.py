# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

import logging
import json

from django.http.response import *
from django.shortcuts import *
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db.transaction import atomic

from api.models import *

LOG = logging.getLogger(__name__)


def __get_auth_token(request):
    """
    Returns the AuthenticationToken for the API user. If the user is logged in the user token is returned,
    otherwise the token is fetched using the `auth_key` which is provided as an API parameter.

    :param request: The request calling the API
    :return: The AuthenticationToken if a user was found, None otherwise
    """

    # Return the user token for logged-in users
    if request.user.is_authenticated:
        return request.user.auth_token

    # Otherwise return the token from the provided key
    auth_key = __get_api_param(request, "auth_key")
    if not auth_key:
        return None

    try:
        auth_token = AuthenticationToken.objects.get(key=auth_key)
    except ObjectDoesNotExist:
        return None
    else:
        return auth_token


def __nice_json_response(container):
    """
    Create a pretty-printed JSON out of container. Always use this
    to generate the response which will be sent to the API Client.

    :param container: A container, containing everything that should be wrapped into a JSON string
    :return: A HttpResponse with set header and the JSON contents
    """

    response = HttpResponse(json.dumps(list(container), indent=4, cls=DjangoJSONEncoder))
    response['Content-Type'] = 'text/json'

    return response


def __get_api_param(request, param):
    """
    Get the value that was provided for the API. Checks first if the header
    was set as an HTTP Header and returns its value if so. Otherwise it will check
    if the parameter was set as a HTTP GET parameter.

    :param request: The request where the parameters should be set
    :param param: The parameter we are looking for
    :return: The value of the parameter as a string
    """

    param_http_header_name = 'HTTP_DS_API_'+param.upper()
    if param_http_header_name in request.META:
        return request.META[param_http_header_name]
    elif param in request.GET:
        return request.GET[param]
    else:
        return None



### API VIEWS ###
#################

def get_sensors(request):
    """
    Get a list of all sensors accessible by this token

    :param request: The django.http request
    :return: list of all accessible sensors
    """

    auth_token = __get_auth_token(request)

    if not auth_token:
        return HttpResponseBadRequest()

    sensors = auth_token.get_read_allowed_sensors.values()
    return __nice_json_response(list(sensors))



@atomic
def get_sensorpings(request, sensor_id):
    """
    Get a list of all sensorpings of a given sensor
    :param request: The django.http request
    :param sensor_id: The ID of the sensor
    :return: list of the all sensorpings of the given sensor as a JSON string
    """

    auth_token = __get_auth_token(request)
    if not auth_token:
        return HttpResponseBadRequest()


    all_sensors = auth_token.get_read_allowed_sensors


    # Todo: Write a function for this!
    try:
        sensor = all_sensors.filter(id=sensor_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden()


    # Prepare to iterate over all SensorPings
    response_list = []
    i = 0

    sensorpings = SensorPing.objects.filter(sensor=sensor).order_by("-time").pre

    # Check if the number of sensorpings has been limited by the API call
    sensorpings_limit = __get_api_param(request, "limit")
    if sensorpings_limit:
        sensorpings = sensorpings[:int(sensorpings_limit)]


    for i in range(0, len(sensorpings)):
        sp = sensorpings.values()[i]
        sp_dict = sp

        data = []

        # Loop through all SensorPingData objects that are available for this single SensorPing
        # to collect single `spd_objects` representing a SensorPingData object storing additional information
        # like the data_type so the webinterface is capable of interpreting the data correctly.
        sensorping_data = sensorpings[i].data.all().order_by("field__name").select_related("field")
        for sp in sensorping_data:
            spd_object = {}
            spd_object['id'] = sp.id
            spd_object['field_name'] = sp.field.name
            spd_object['data_type'] = sp.field.data_type
            spd_object['data'] = sp.data

            # Append this spd_object to the SensorPing
            data.append(spd_object)

        # Set the data of this SensorPing to the collected spd_objects
        sp_dict['data'] = data

        # Append this SensorPing to the response list. (The response list shows *all* SensorPings)
        response_list.append(sp_dict)

    return __nice_json_response(response_list)



@atomic
def ping_sensor(request, sensor_id):
    auth_token = __get_auth_token(request)

    all_sensors = auth_token.get_write_allowed_sensors
    try:
        sensor = all_sensors.get(id=sensor_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden()


    new_sensor_ping = SensorPing.objects.create(sensor=sensor)

    # Loop over all SensorPingDataFields to see if there is any important data provided
    for data_field in sensor.data_fields.all():
        value = __get_api_param(request, data_field.name)
        if value:
            SensorPingData.objects.create(sensor_ping=new_sensor_ping, field=data_field, data=value)
            LOG.warning("Received additional data for '{}': '{}'".format(data_field.name, value))



    return HttpResponse("Ok")

