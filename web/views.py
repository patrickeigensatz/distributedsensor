# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

import logging

from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.http.response import HttpResponseBadRequest, HttpResponse

from api.models import Sensor

LOG = logging.getLogger(__name__)


def login(request):
    """
    A view to prompt the user for the username/password and log the user in
    :param request: The django.http.requests.HttpRequest
    :return: A HttpResponse
    """

    if request.user.is_authenticated:
        redirect("dashboard")


    if not request.POST:
        return render(request, 'web/login.html', locals())

    else:
        if not ("username" in request.POST and "password" in request.POST):
            return HttpResponseBadRequest()

        user = authenticate(request, username=request.POST["username"], password=request.POST["password"])
        if user != None:
            LOG.info("User {} successfully logged in!".format(request.POST["username"]))
            django_login(request, user)
            return redirect("dashboard")
        else:
            LOG.info("User {} failed to login!".format(request.POST["username"]))
            url = reverse("login") + "?failed=1"
            return redirect(url)


def logout(request):
    django_logout(request)
    return redirect("login")


@login_required
def dashboard(request):
    return render(request, "web/dashboard.html", locals())

@login_required
def sensor(request, sensor_id):
    sensor = get_object_or_404(Sensor, id=sensor_id)
    return render(request, "web/sensor.html", locals())

