# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-10 22:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20170910_1833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authenticationtoken',
            name='read_permission',
            field=models.ManyToManyField(blank=True, default=None, related_name='read_access_tokens', to='api.Sensor'),
        ),
        migrations.AlterField(
            model_name='authenticationtoken',
            name='write_permission',
            field=models.ManyToManyField(blank=True, default=None, related_name='write_access_tokens', to='api.Sensor'),
        ),
    ]
