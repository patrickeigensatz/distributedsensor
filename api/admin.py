# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

from django.contrib import admin
from api.models import *

# Register your models here.
admin.site.register(AuthenticationToken)
