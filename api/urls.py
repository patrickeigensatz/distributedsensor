# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

from django.conf.urls import url

from api.views import get_sensors, get_sensorpings, ping_sensor

urlpatterns = [
    # Read
    url(r'getSensors', get_sensors, name='get_sensors'),
    url(r'getSensorPings/(\d+)', get_sensorpings, name='get_sensorpings'),

    # Write
    url(r'ping/(\d+)', ping_sensor, name='ping_sensor'),
]