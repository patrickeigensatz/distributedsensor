# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

from django.test import TestCase, Client
from api.models import *

# Create your tests here.
class SensorPingTest(TestCase):
    def setUp(self):
        Sensor.objects.create(name="TestSensor", auth_token="asdf1234")

    def test_sensor_ping(self):
        c = Client()
        response = c.get("/ping/1/asdf1234")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"Ok")
        self.assertEqual(SensorPing.objects.all().count(), 1)


    def test_sensor_ping_status(self):
        c = Client()
        response = c.get("/ping/1/asdf1234?status=load%3D0.6")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"Ok")
        self.assertEqual(SensorPing.objects.filter(status_text="load=0.6").count(), 1)
