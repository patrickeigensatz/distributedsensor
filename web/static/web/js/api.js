var sensor_id = 0;
var all_pings = [];
var sensorpings_limit = 50;
var timestamp_format = "YYYY-MM-DD HH:mm";



function getSensors(callback){
    $.get("/api/getSensors", callback);
}


function renderSensors(sensors){
    if(sensors.length == 0){
        $("#notFound").html("No Sensors found");
        return;
    }

    var l = "<ul>";
    for(var i=0; i<sensors.length; i++){
        l += '<li><a href="/web/sensor/' + sensors[i].id + '/" style="text-decoration: none;">' + sensors[i].name + '</li>';
    }
    l += "</ul>";

    $("#sensorList").html(l);
}


function getSensorPings(s_id, callback){
    sensor_id = s_id
    $.get("/api/getSensorPings/" + s_id.toString() + "?limit=" + sensorpings_limit, callback)
}


function renderSensorPingToString(ping){
    var date = moment(new Date(ping.time)).format(timestamp_format);

    var result = '<div class="sensorping" data-toggle="collapse" data-target="#ping' + ping.id + '"><div class="h4">' + date;
    result += '</div><div class="collapse" id="ping' + ping.id + '">';

    // Loop over all data and display the fields in their own row
    for(var i=0; i<ping.data.length; i++){
        // debugger;
        result += '<div class="row"><div class="col-xs-9">' + ping.data[i].field_name + '</div><div class="col-xs-3">' + ping.data[i].data + '</div></div>'
    }

    result += '</div></div>';

    return result;
}


function renderSensorPings(pings){
    all_pings = pings;

    var l = "";
    var timestamps = [];
    var data = [];

    if(pings.length == 0){
        console.log("No SensorPings found for this sensor");
        $("#notFound").html("No SensorPings found");
        return;
    }

    // Collect all NUMeric SensorPingData
    var all_spd_data = {}

    // Set the bucket_timestamp to the first value so there won't be an "empty/zero-length" bucket
    var bucket_timestamp = moment(new Date(pings[0].time)).format(timestamp_format);
    var bucket_count = 0;
    for(var i=0; i<pings.length; i++){
        var t = moment(new Date(pings[i].time));
        var timestamp = t.format(timestamp_format);

        // Prepare numeric SensorPingData
        for(var field_index=0; field_index<pings[i].data.length; field_index++){
            field = pings[i].data[field_index];

            console.log(field)
            if(field.data_type == "NUM"){

                // Create the SensorPingData statistics array the first time this Field appears
                if(all_spd_data[field.field_name] == undefined){
                    all_spd_data[field.field_name] = {};
                    all_spd_data[field.field_name]["timestamps"] = [];
                    all_spd_data[field.field_name]["data"] = [];
                }

                all_spd_data[field.field_name]["timestamps"].push(timestamp);
                all_spd_data[field.field_name]["data"].push(parseFloat(field.data));
            }
        }

        // Append the HTML to the list of SensorPings
        l += renderSensorPingToString(pings[i]);

        if(timestamp == bucket_timestamp){
            // Still the same day/hour/minute
            bucket_count += 1;
        } else {
            // A new day/hour/minute
            // Push the old timestamp and count and reset the bucket
            timestamps.push(bucket_timestamp);
            data.push(bucket_count);

            bucket_timestamp = timestamp;
            bucket_count = 1;

        }
    }


    // Also push the last bucket
    timestamps.push(bucket_timestamp);
    data.push(bucket_count);


    // Reverse the data for the plot
    timestamps = timestamps.reverse();
    data = data.reverse();


    var ctx = $("#sensorPingChart");
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: timestamps,
            datasets: [
                {
                    data: data,
                    label: "Number of SensorPings",

                    borderColor: [
                        'rgba(0, 255, 0, 0.9)'
                    ],
                    backgroundColor: [
                        'rgba(0, 255, 0, 0.2)'
                    ]
                }
            ]
        },
        options: {
            elements: {
                line: {
                    tension: 0
                }
            },

            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                    }
                }]
            }
        }
    });
    $("#sensorPingsList").html(l);



    // Create a chart for every captured SensorPingDataField
    for(var spd_field in all_spd_data){
        $("#sensorPingDataCharts").append('<div class="row"><div class="col-xs-12"><canvas id="sensorPingDataChart' + spd_field + '"></canvas></div></div>')

        var ctx = $('#sensorPingDataChart' + spd_field);
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: all_spd_data[spd_field]["timestamps"],
                datasets: [
                    {
                        data: all_spd_data[spd_field]["data"],
                        label: spd_field,

                        borderColor: [
                            'rgba(0, 255, 0, 0.9)'
                        ],
                        backgroundColor: [
                            'rgba(0, 255, 0, 0.2)'
                        ]
                    }
                ]
            },
            options: {
                elements: {
                    line: {
                        tension: 0
                    }
                },

                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }]
                }
            }
        });
    }

}




function updateSensorPingLimit(){
    sensorpings_limit = $("#sensorpings_limit").val();
    getSensorPings(sensor_id, renderSensorPings);

    // Remove all SensorPingDataCharts to avoid having them multiple times
    $("#sensorPingDataCharts").html("");
}



function updateTimestampFormat(){
    timestamp_format = $("#timestamp_format").val();

    // Remove all SensorPingDataCharts to avoid having them multiple times
    $("#sensorPingChart").html("");
    $("#sensorPingDataCharts").html("");

    renderSensorPings(all_pings);
}