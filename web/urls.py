# Copyright (C) 2017  Patrick Eigensatz <patrick.eigensatz@gmail.com>
# Distributed under the MIT license. See the LICENSE file for more information.

from django.conf.urls import url

from web.views import *

urlpatterns = [
    url(r'^$', login),
    url(r'login', login, name='login'),
    url(r'logout', logout, name='logout'),
    url(r'dashboard', dashboard, name='dashboard'),
    url(r'sensor/(\d+)', sensor, name='sensor'),
]