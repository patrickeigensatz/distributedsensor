# DistributedSensor Specifications
### A flexible, easy-to-integrate server status and monitoring software
###### v0.1

## Setup
### Master Node
The master node runs DistributedSensor and keeps a connection to a database, where all
the pings are stored. It also provides a webinterface to analyze the collected data.
Furthermore it allows to run custom scripts or take other action as soon as a specified
"error threshold" for a server has been exceeded.

At the moment, there is **one** master node but there can be as many sensor nodes
as you like.

### Sensor Nodes
The nodes that are under surveillance are called "Sensor Nodes". A sensor is a script that
runs periodically (might be scheduled) to continously report the status of the sensor node
to the master node. The sensor node can send additional payload such as RAM usage, number
of logged-in users, etc., that will be stored in the database the master node is connected to.

A ping can be sent from every script. DistributedSensor provides examples but there is no need
to run another master server and setup a database there as well.


## Ping-related models
# *Todo* Add information about the Sensor model
# *Todo* Add information about the SensorPing model



## REST API
### Authentication Tokens
The API is covered by an authentication system, providing the necessary permissions for a Sensor
to send a SensorPing but also for the webinterface or whatever applications you like to read all
the SensorPings of a given Sensor.

An authentication token saves the following data

*Parameter*        | *Necessary?* | *Description*
-------------------|---------------|-------------
Name               |     Yes       | Mainly for the webinterface, therefore a human readable name (E.g. `SSH Server`)
Key                |     Yes       | The authentication token's key that allows the sensor to report data (E.g. `d9981358994b6cb2bede51267b35496946c253fa7b1b0bf0d6475cb6be27bf2a`)
read_permission    |     No        | A list of sensors this token has read-only access to
write_permission   |     No        | A list of sensors this token has write-only access to


### The `ping` module
The `ping ` module is used to send a simple ping or a payload to the master server.

URL: `/ping/SENSOR_ID/AUTH_TOKEN[?payload=PAYLOAD]`

*Parameter*   | *Necessary?* | *Description*
--------------|--------------|--------------
SENSOR_ID     |     Yes      | The ID of Sensor that is reporting data. (E.g. `12`)
AUTH_TOKEN    |     Yes      | The authentication token key that allows the sensor to report data (E.g. `d9981358994b6cb2bede51267b35496946c253fa7b1b0bf0d6475cb6be27bf2a`)
payload       |     No       | Additional data that should be saved along with this SensorPing (E.g. `"sysload=0.42"`)
 

### The `api` module
Via the API it is possible to read all the collected data and get a JSON response
to further analyze or display the data.


## Webinterface
The webinterface should be able to
- List all Sensors
- List all recent SensorPings
- List all Pings of a given Sensor
- Create users and manage permissions. *Users get their own API key, so their permissions should be updated there*
  and the webinterface can make use of the API.

